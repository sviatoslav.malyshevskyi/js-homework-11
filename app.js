'use strict';

/* Remove blue color from all buttons */
function clearPrevious() {
    if (document.querySelector('.active')) {
        document.querySelector('.active').classList.remove('active');
    }
}

/* Add blue color to the pressed button */
window.addEventListener('keydown', function (event) {
    switch (event.key) {
        case 'Enter':
            clearPrevious();
            document.getElementById('btn-Enter').classList.add('active');
            break;
        case 's':
            clearPrevious();
            document.getElementById('btn-s').classList.add('active');
            break;
        case 'e':
            clearPrevious();
            document.getElementById('btn-e').classList.add('active');
            break;
        case 'o':
            clearPrevious();
            document.getElementById('btn-o').classList.add('active');
            break;
        case 'n':
            clearPrevious();
            document.getElementById('btn-n').classList.add('active');
            break;
        case 'l':
            clearPrevious();
            document.getElementById('btn-l').classList.add('active');
            break;
        case 'z':
            clearPrevious();
            document.getElementById('btn-z').classList.add('active');
            break;
    }
});